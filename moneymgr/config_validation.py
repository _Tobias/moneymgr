from moneymgr.helpers import get_accounts_with

def validate(options, accounts, verbose):
    input_accounts = get_accounts_with(accounts, "is_input_account")
    support_accounts = get_accounts_with(accounts, "is_support_account")
    overflow_accounts = get_accounts_with(accounts, "is_overflow_account")
    monthly_filled_accounts = get_accounts_with(accounts, "monthly_amount")
    ceiled_accounts = get_accounts_with(accounts, "ceiling_no_of_months")
    payoff_accounts = get_accounts_with(accounts, "is_payoff_account")

    good = True

    if len(input_accounts) != 1:
        if verbose:
            good = False
            print("FAIL: You must have exactly one input account")
        else:
            return False

    if len(overflow_accounts) > 1:
        if verbose:
            good = False
            print("FAIL: You may not have more than one overflow account")
        else:
            return False

    if len(overflow_accounts) == 1 and options["drain_input"] and overflow_accounts[0] == input_accounts[0]:
        if verbose:
            good = False
            print("FAIL: Input account is also overflow account AND drain input is enabled")
        else:
            return False

    if len(payoff_accounts) > 1:
        if verbose:
            good = False
            print("FAIL: You may not have more than one payoff account")
        else:
            return False

    for account in ceiled_accounts:
        if account["monthly_amount"] == 0:
            if verbose:
                good = False
                print("FAIL: There are one or more ceiled accounts with monthly amount set to 0.")
            else:
                return False

    for account in monthly_filled_accounts:
        for support_account in support_accounts:
            if support_account["id"] == account["id"]:
                if verbose:
                    good = False
                    print("FAIL: A support account may not be a monthly filled account")
                else:
                    return False

        for input_account in input_accounts:
            if input_account["id"] == account["id"]:
                if verbose:
                    good = False
                    print("FAIL: An input account may not also be a monthly filled account")
                else:
                    return False
    
    if verbose:
        print("Number of monthly filled accounts: " + str(len(monthly_filled_accounts)))
        print("Number of support accounts: " + str(len(support_accounts)))

    return good
