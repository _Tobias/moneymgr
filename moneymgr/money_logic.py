from bunq.sdk.model.generated import endpoint
from bunq.sdk.model.generated.object_ import Pointer, Amount

from moneymgr.helpers import get_accounts_with, sanitize_amount, list_payments
from moneymgr.config_validation import validate
from datetime import datetime
import time

def send_payments(payment_descriptors):
    i = 1
    for payment_descriptor in payment_descriptors:
        print("Sending transfer " + str(i) + "/" + str(len(payment_descriptors)) + "...")
        payment = payment_descriptor["payment"]
        endpoint.Payment.create(
            counterparty_alias=payment._counterparty_alias_field_for_request,
            amount=payment._amount_field_for_request,
            description=payment._description_field_for_request,
            monetary_account_id=payment_descriptor["monetary_account_id"]
        )
        
        time.sleep(2)

        i += 1

def process_money(options, payoff, accounts, confirm):
    input_accounts = get_accounts_with(accounts, "is_input_account")
    support_accounts = get_accounts_with(accounts, "is_support_account")
    overflow_accounts = get_accounts_with(accounts, "is_overflow_account")
    payoff_accounts = get_accounts_with(accounts, "is_payoff_account")
    monthly_filled_accounts = get_accounts_with(accounts, "monthly_amount")
    ceiled_accounts = get_accounts_with(accounts, "ceiling_no_of_months")
    
    if not validate(options, accounts, False):
        raise RuntimeError("Settings not valid, run with --validate to get more info")
    
    total_amount = 0.0
    support_balance = 0.0

    payment_descriptors = []

    for account in support_accounts:
        support_balance += account["balance"]

    for account in monthly_filled_accounts:
        total_amount += account["monthly_amount"]
    
    if input_accounts[0]["balance"] + support_balance < total_amount:
        print("FAIL: the input account + support accounts do not have sufficient balance (" + str(input_accounts[0]["balance"] + support_balance) + ") to supply the total amount (" + str(total_amount) + ")")
        print()
        exit()

    today = str(datetime.today().date())

    for account in monthly_filled_accounts:
        account["history"].append({
            "date": today,
            "monthly_amount": account["monthly_amount"],
            "ceiling_no_of_months": account["ceiling_no_of_months"],
            "balance_before_deposit": account["balance"]
        })

        rest = account["monthly_amount"]
        while support_balance > 0 and rest > 0:
            support = get_accounts_with(support_accounts, "balance")[0]

            paid = sanitize_amount(min(rest, support["balance"]))

            payment_descriptors.append({
                "monetary_account_id": support["id"],
                "payment": endpoint.Payment(
                    counterparty_alias=Pointer("IBAN", account["iban"], name="T. Sijtsma"),
                    amount=Amount(str(paid), "EUR"),
                    description="moneymgr"
                )
            })
            support_balance = sanitize_amount(support_balance - paid)
            support["balance"] = sanitize_amount(support["balance"] - paid)
            account["balance"] = sanitize_amount(account["balance"] + paid)
            rest = sanitize_amount(rest - paid)

        if rest > 0:
            payment_descriptors.append({
                "monetary_account_id": input_accounts[0]["id"],
                "payment": endpoint.Payment(
                    counterparty_alias=Pointer("IBAN", account["iban"], name="T. Sijtsma"),
                    amount=Amount(str(rest), "EUR"),
                    description="moneymgr"
                )
            })

            account["balance"] = sanitize_amount(account["balance"] + rest)
            input_accounts[0]["balance"] = sanitize_amount(input_accounts[0]["balance"] - rest)

    if len(overflow_accounts) == 1 and len(ceiled_accounts) > 0:        
        for account in ceiled_accounts:
            if account["balance"] > sanitize_amount(account["monthly_amount"] * account["ceiling_no_of_months"]):
                payment_descriptors.append({
                    "monetary_account_id": account["id"],
                    "payment": endpoint.Payment(
                        counterparty_alias=Pointer("IBAN", overflow_accounts[0]["iban"], name="T. Sijtsma"),
                        amount=Amount(str(sanitize_amount(account["balance"] - account["monthly_amount"] * account["ceiling_no_of_months"])), "EUR"),
                        description="moneymgr overflow"
                    )
                })

    if options["drain_input"] and len(overflow_accounts) == 1 and input_accounts[0]["balance"] > 0:
        payment_descriptors.append({
            "monetary_account_id": input_accounts[0]["id"],
            "payment": endpoint.Payment(
                counterparty_alias=Pointer("IBAN", overflow_accounts[0]["iban"], name="T. Sijtsma"),
                amount=Amount(str(input_accounts[0]["balance"]), "EUR"),
                description="moneymgr drain"
            )
        })
        overflow_accounts[0]["balance"] = sanitize_amount(overflow_accounts[0]["balance"] + input_accounts[0]["balance"])
        input_accounts[0]["balance"] = 0.0
    
    if len(payoff_accounts) == 1 and payoff_accounts[0]["balance"] >= payoff["threshold"] + payoff["min_amount"]:
        payment_descriptors.append({
            "monetary_account_id": payoff_accounts[0]["id"],
            "payment": endpoint.Payment(
                counterparty_alias=Pointer("IBAN", payoff["target_iban"], name="DUO"),
                amount=Amount(str(sanitize_amount(payoff_accounts[0]["balance"] - payoff["threshold"])), "EUR"),
                description=payoff["description"]
            )
        })
        

    if confirm:
        send_payments(payment_descriptors)
    else:
        # undo history entries when not confirming
        for account in monthly_filled_accounts:
            account["history"] = account["history"][0:len(account["history"]) - 1]
        list_payments(accounts, payment_descriptors)
        print("Run with --confirm to actually make these transfers")
        print()