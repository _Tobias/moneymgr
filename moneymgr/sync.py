from bunq.sdk.client import Pagination
from bunq.sdk.context import ApiContext
from bunq.sdk.context import BunqContext
from bunq.sdk.model.generated import endpoint

from moneymgr.helpers import get_iban

def synchronize(accounts):
    api_context = ApiContext.restore("bunq-production.conf")
    api_context.ensure_session_active()
    api_context.save()

    BunqContext.load_api_context(api_context)

    pagination = Pagination()
    pagination.count = 25
    all_monetary_account_bank = endpoint.MonetaryAccountBank.list(pagination.url_params_count_only).value
    all_monetary_account_savings = endpoint.MonetaryAccountSavings.list(pagination.url_params_count_only).value
    all_monetary_account = all_monetary_account_bank + all_monetary_account_savings

    for monetary_account in all_monetary_account:
        if monetary_account.status == "ACTIVE":
            local_account = None
            for account in accounts:
                if get_iban(monetary_account) == account["iban"]:
                    local_account = account
                    account["description"] = monetary_account.description
                    account["balance"] = float(monetary_account.balance.value)
            if local_account is None:
                accounts.append({
                    "id": monetary_account.id_,
                    "iban": get_iban(monetary_account),
                    "balance": 0.0,
                    "is_input_account": False,
                    "is_overflow_account": False,
                    "is_support_account": False,
                    "is_payoff_account": False,
                    "is_hidden_account": False,
                    "monthly_amount": 0.0,
                    "ceiling_no_of_months": 0,
                    "description": monetary_account.description,
                    "history": []
                })

    ditch = []
    for account in accounts:
        found = False
        for monetary_account in all_monetary_account:
            if monetary_account.status == "ACTIVE" and get_iban(monetary_account) == account["iban"]:
                found = True
        if not found:
            ditch.append(account)

    for item in ditch:
        accounts.remove(item)