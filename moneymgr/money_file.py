from json import dump, load
from os.path import isfile

def load_money_file(path):
    if not isfile(path):
        dump({
            "accounts": [],
            "payoff": {
                "threshold": 8000.0,
                "min_amount": 45.0,
                "target_iban": "NL45INGB0705001903",
                "description": "123456789ILS"
            },
            "options": {
                "drain_input": False
            }
        }, open(path, "w"))
    return load(open(path, "r"))