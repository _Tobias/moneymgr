from bunq.sdk.model.generated import endpoint

def get_iban(account):
    for alias in account.alias:
        if alias.type_ == "IBAN":
            return alias.value

def get_type(account):
    s = ""
    if account["is_input_account"]:
        s += "I"
    else:
        s += "-"
    if account["is_overflow_account"]:
        s += "O"
    else:
        s += "-"
    if account["is_support_account"]:
        s += "S"
    else:
        s += "-"
    if account["is_payoff_account"]:
        s += "P"
    else:
        s += "-"
    return s

def set_account_option(accounts, account_id, option, value):
    for account in accounts:
        if account["id"] == account_id:
            account[option] = value
            return
    raise ValueError("Account ID not found")

def get_accounts_with(accounts, prop):
    l = []
    for account in accounts:
        if account[prop]:
            l.append(account)
    return l

def get_account_by_id(accounts, id):
    for account in accounts:
        if account["id"] == id:
            return account

def get_account_by_iban(accounts, iban):
    for account in accounts:
        if account["iban"] == iban:
            return account
    return None

def list_accounts(accounts, show_hidden):
    print("ID\tIBAN\t\t\tType\tBalance\tMonthly\tCeiling\tDescription")
    for account in accounts:
        if show_hidden or not account["is_hidden_account"]:
            print(str(account["id"]) + "\t" + account["iban"] + "\t" + get_type(account) + "\t" + str(account["balance"]) + "\t" + str(account["monthly_amount"]) + "\t" + str(account["ceiling_no_of_months"]) + " mo.\t" + account["description"])
    print()

def list_stats(accounts):
    monthly_amount = 0.0
    for account in get_accounts_with(accounts, "monthly_amount"):
        monthly_amount = sanitize_amount(monthly_amount + account["monthly_amount"])
    print("Total monthly amount: " + str(monthly_amount))
    print()

def list_payments(accounts, payment_descriptors):
    last_account_announced = -1
    for payment_descriptor in sorted(payment_descriptors, key=lambda d: d["monetary_account_id"]):
        if payment_descriptor["monetary_account_id"] != last_account_announced:
            if last_account_announced != -1:
                print()
            last_account_announced = payment_descriptor["monetary_account_id"]
            payer = get_account_by_id(accounts, payment_descriptor["monetary_account_id"])
            print(payer["description"] + " (" + payer["iban"] + ") is paying the following:")
        payment = payment_descriptor["payment"]
        receiver = get_account_by_iban(accounts, payment._counterparty_alias_field_for_request._value_field_for_request)
        if receiver is None:
            receiver = {
                "iban": payment._counterparty_alias_field_for_request._value_field_for_request,
                "description": "External payoff account"
            }
        print("- " + str(payment._amount_field_for_request._value_field_for_request) + " to " + receiver["description"] + " (" + receiver["iban"] + ")" + (" (overflow payment)" if payment._description_field_for_request == "moneymgr overflow" else ""))
    print()

def sanitize_amount(amount):
    return round(amount * 100) / 100
