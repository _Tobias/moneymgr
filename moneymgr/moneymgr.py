from json import dump
from moneymgr.helpers import list_accounts, list_stats
from moneymgr.config_validation import validate
from moneymgr.arguments import args, process_setting_args
from moneymgr.sync import synchronize
from moneymgr.money_file import load_money_file
from moneymgr.money_logic import process_money

# Load money file and create if needed
money = load_money_file(args.money_file)

# Process any options that set a basic parameter
setting_arg_processed = process_setting_args(money["payoff"], money["options"], money["accounts"])

if not setting_arg_processed:
    if args.list_stats:
        if args.sync:
            synchronize(money["accounts"])
        
        list_stats(money["accounts"])
    elif args.validate:
        if args.sync:
            synchronize(money["accounts"])

        good = validate(money["options"], money["accounts"], True)

        if good:
            print("SUCCESS: Settings validated")
        else:
            print("FAIL: Settings not valid")
        print()

    elif args.process:
        synchronize(money["accounts"])
        
        process_money(money["options"], money["payoff"], money["accounts"], args.confirm)

        if args.confirm:
            list_accounts(money["accounts"], args.show_hidden)
            dump(money, open("money.json", "w"))
    else:
        if args.sync:
            synchronize(money["accounts"])
            dump(money, open("money.json", "w"))

        list_accounts(money["accounts"], args.show_hidden)
else:
    dump(money, open("money.json", "w"))
