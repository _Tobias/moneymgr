import argparse
from json import dump, load
from os.path import isfile
from moneymgr.helpers import set_account_option, get_accounts_with

parser = argparse.ArgumentParser("Manage money.")
parser.add_argument("--money-file", metavar="FILE", help="The JSON file on which to operate.", type=str, default="money.json")
parser.add_argument("--account", metavar="ID", help="Perform action on an account ID.", type=int)
parser.add_argument("--enable-drain-input", help="Enable automatically draining the input account into overflow account after payouts.", action='store_true')
parser.add_argument("--disable-drain-input", help="Disable automatically draining the input account into overflow account after payouts.", action='store_true')
parser.add_argument("--get-drain-input", help="See whether automatically draining the input account into overflow account after payouts is enabled.", action='store_true')
parser.add_argument("--list-payoff-config", help="List automatic payoff configuration.", action='store_true')
parser.add_argument("--set-payoff-iban", help="Set automatic payoff target IBAN.", type=str)
parser.add_argument("--set-payoff-threshold", help="Set the payoff amount threshold.", type=float)
parser.add_argument("--set-payoff-min-amount", help="Set the minimal payoff amount.", type=float)
parser.add_argument("--set-payoff-description", help="Set payoff transfer description.", type=str)
parser.add_argument("--set-monthly-amount", metavar='AMOUNT', help="Set monthly amount of an account.", type=float)
parser.add_argument("--set-ceiling", metavar='NR_MONTHS', help="Set the no. of months the amount can accumulate before overflowing.", type=int)
parser.add_argument("--set-input-account", help="Set as input account.", action='store_true')
parser.add_argument("--set-overflow-account", help="Set as overflow account.", action='store_true')
parser.add_argument("--set-support-account", help="Set as support account.", action='store_true')
parser.add_argument("--set-payoff-account", help="Set as payoff account.", action='store_true')
parser.add_argument("--set-hidden-account", help="Set as hidden account.", action='store_true')
parser.add_argument("--unset-input-account", help="Unset as input account.", action='store_true')
parser.add_argument("--unset-overflow-account", help="Unset as overflow account.", action='store_true')
parser.add_argument("--unset-support-account", help="Unset as support account.", action='store_true')
parser.add_argument("--unset-payoff-account", help="Unset as payoff account.", action='store_true')
parser.add_argument("--unset-hidden-account", help="Unset as hidden account.", action='store_true')
parser.add_argument("--list-stats", help="Show statistics.", action='store_true')
parser.add_argument("--sync", help="Force Bunq synchronization.", action='store_true')
parser.add_argument("--show-hidden", help="Show hidden accounts as well.", action='store_true')
parser.add_argument("--validate", help="Validate current settings.", action='store_true')
parser.add_argument("--process", help="Do the monthly run.", action='store_true')
parser.add_argument("--confirm", help="Confirm, to be used with --process.", action='store_true')

args = parser.parse_args()

def process_setting_args(payoff, options, accounts):
    consumed = False

    if args.enable_drain_input:
        consumed = True
        options["drain_input"] = True
        print("Automatically draining input account into overflow account after payouts enabled")
        print()
    elif args.disable_drain_input:
        consumed = True
        options["drain_input"] = False
        print("Automatically draining input account into overflow account after payouts disabled")
        print()
    elif args.get_drain_input:
        consumed = True
        print("Automatically draining input account into overflow account after payouts enabled: " + str(options["drain_input"]))
        print()
    elif args.list_payoff_config:
        consumed = True
        payoff_accounts = get_accounts_with(accounts, "is_payoff_account")
        if len(payoff_accounts) == 0:
            print("Automatic payoff is disabled")
        elif len(payoff_accounts) == 1:
            print("Automatic payoff is enabled on account " + payoff_accounts[0]["description"] + " (" + payoff_accounts[0]["iban"] + ")")
            print("Automatic payoff threshold: " + str(payoff["threshold"]))
            print("Automatic payoff minimal amount: " + str(payoff["min_amount"]))
            print("Automatic payoff target IBAN: " + payoff["target_iban"])
            print("Automatic payoff description: " + payoff["description"])
        
        print()


    elif args.set_payoff_iban is not None:
        consumed = True
        payoff["target_iban"] = args.set_payoff_iban
        print("Automatic payoff IBAN set to " + args.set_payoff_iban)
        print()
    elif args.set_payoff_threshold is not None:
        consumed = True
        payoff["threshold"] = args.set_payoff_threshold
        print("Automatic payoff threshold set to " + str(args.set_payoff_threshold))
        print()
    elif args.set_payoff_min_amount is not None:
        consumed = True
        payoff["min_amount"] = args.set_payoff_min_amount
        print("Automatic payoff minimal amount set to " + str(args.set_payoff_min_amount))
        print()
    elif args.set_payoff_description is not None:
        consumed = True
        payoff["description"] = args.set_payoff_description
        print("Automatic payoff description set to " + args.set_payoff_description)
        print()

    elif args.account is not None:
        if args.set_ceiling is not None:
            consumed = True
            print("Set ceiling of account " + str(args.account) + " to " + str(args.set_ceiling) + " months")
            print()
            set_account_option(accounts, args.account, "ceiling_no_of_months", args.set_ceiling)
        elif args.set_input_account:
            consumed = True
            print("Set account " + str(args.account) + " as input account")
            print()
            set_account_option(accounts, args.account, "is_input_account", True)
        elif args.set_overflow_account:
            consumed = True
            print("Set account " + str(args.account) + " as overflow account")
            print()
            set_account_option(accounts, args.account, "is_overflow_account", True)
        elif args.set_support_account:
            consumed = True
            print("Set account " + str(args.account) + " as support account")
            print()
            set_account_option(accounts, args.account, "is_support_account", True)
        elif args.set_payoff_account:
            consumed = True
            print("Set account " + str(args.account) + " as payoff account")
            print()
            set_account_option(accounts, args.account, "is_payoff_account", True)
        elif args.set_hidden_account:
            consumed = True
            print("Set account " + str(args.account) + " as hidden account")
            print()
            set_account_option(accounts, args.account, "is_hidden_account", True)
        elif args.unset_input_account:
            consumed = True
            print("Unset account " + str(args.account) + " as input account")
            print()
            set_account_option(accounts, args.account, "is_input_account", False)
        elif args.unset_overflow_account:
            consumed = True
            print("Unset account " + str(args.account) + " as overflow account")
            print()
            set_account_option(accounts, args.account, "is_overflow_account", False)
        elif args.unset_support_account:
            consumed = True
            print("Unset account " + str(args.account) + " as support account")
            print()
            set_account_option(accounts, args.account, "is_support_account", False)
        elif args.unset_payoff_account:
            consumed = True
            print("Unset account " + str(args.account) + " as payoff account")
            print()
            set_account_option(accounts, args.account, "is_payoff_account", False)
        elif args.unset_hidden_account:
            consumed = True
            print("Unset account " + str(args.account) + " as hidden account")
            print()
            set_account_option(accounts, args.account, "is_hidden_account", False)
        elif args.set_monthly_amount is not None:
            consumed = True
            print("Set monthly amount of account " + str(args.account) + " to " + str(args.set_monthly_amount))
            print()
            set_account_option(accounts, args.account, "monthly_amount", args.set_monthly_amount)
        
    return consumed
    
