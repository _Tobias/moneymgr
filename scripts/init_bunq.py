from bunq.sdk.client import Pagination
from bunq.sdk.context import ApiContext
from bunq.sdk.context import ApiEnvironmentType
from bunq.sdk.context import BunqContext
from bunq.sdk.model.generated import endpoint

CONTEXT_FILE = "bunq-production.conf"

api_context = ApiContext.restore(CONTEXT_FILE)
api_context.ensure_session_active()
api_context.save(CONTEXT_FILE)
BunqContext.load_api_context(api_context)
