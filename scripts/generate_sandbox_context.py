from os.path import isfile
import argparse
import socket
import json
import requests

from bunq.sdk.context import ApiContext
from bunq.sdk.context import ApiEnvironmentType
from bunq.sdk.model.generated import endpoint

def generate_new_sandbox_user():
    """
    :rtype: SandboxUser
    """

    url = "https://sandbox.public.api.bunq.com/v1/sandbox-user"

    headers = {
        'x-bunq-client-request-id': "uniqueness-is-required",
        'cache-control': "no-cache",
        'x-bunq-geolocation': "0 0 0 0 NL",
        'x-bunq-language': "en_US",
        'x-bunq-region': "en_US",
    }

    response = requests.request("POST", url, headers=headers)

    if response.status_code is 200:
        response_json = json.loads(response.text)
        return endpoint.SandboxUser.from_json(
            json.dumps(response_json["Response"][0]["ApiKey"]))

    raise BunqException("Could not create new sandbox user.")

parser = argparse.ArgumentParser("Generate a sandbox Bunq API context.")
parser.add_argument("output_filename", metavar="FILE", help="A destination for the context.")

args = parser.parse_args()
generated_user = generate_new_sandbox_user()
print("Generated sandbox user with API key: " + generated_user.api_key)

# ApiContext(ApiEnvironmentType.SANDBOX, generated_user.api_key, socket.gethostname()).save(args.output_filename)

# api_context = ApiContext.restore(args.output_filename)
# api_context.ensure_session_active()
# api_context.save()
